import printers from './printers'
// @todo: implement this ine the back-end also
// @todo: set unique id per stations
export default {
  create (printerId) {
    const storedStations = window.localStorage.getItem('stations')
    const currentStations = storedStations ? JSON.parse(storedStations) : []
    const printer = printers.get(printerId)
    const station = {
      printer,
      materials: []
    }
    currentStations.push(station)
    window.localStorage.setItem('stations', JSON.stringify(currentStations))
  },
  update (printer, material) {
    console.log(material)
    const storedStations = window.localStorage.getItem('stations')
    const currentStations = storedStations ? JSON.parse(storedStations) : []
    const updatedStations = currentStations.map(station => {
      if (station.printer.id === printer.id) {
        station.materials.push(material)
      }

      return station
    })
    window.localStorage.setItem('stations', JSON.stringify(updatedStations))
  },
  get () {
    return JSON.parse(window.localStorage.getItem('stations'))
  }
}
