import m from 'mithril'
import addMaterial from './add-material'

export default {
  addMaterialIsOpened: false,
  view (vnode) {
    const {printer, materials} = vnode.attrs

    return m('.printer-details', [
      m('p', [m('b', 'manufacturer: '), m('span', printer.manufacturer)]),
      m('p', [m('b', 'technology: '), m('span', printer.technology)]),
      m('p', m('b', 'Materials: ')),
      materials.length > 0 ? m('ul', materials.map(material => m('li', [
        m('span.material-color', {style: `background-color: ${material.color}`}),
        m('b', material.quantity),
        ` ${material.name}`
      ]))) : 'N/A',
      m('button.button.button--submit', {onclick: event => {
        vnode.state.addMaterialIsOpened = !vnode.state.addMaterialIsOpened
        event.preventDefault()
        event.stopPropagation()
      }}, 'add material'),
      vnode.state.addMaterialIsOpened ? m(addMaterial, {printer}) : null
    ])
  }
}
