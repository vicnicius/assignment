import m from 'mithril'
import header from './header'
import footer from './footer'

const pageWrapper = {
  view (vnode) {
    return m('main#app', [
      m(header),
      vnode.children,
      m(footer)
    ])
  }
}

export default pageWrapper
