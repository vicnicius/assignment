import m from 'mithril'
import pageWrapper from '../components/page-wrapper'
import printerStation from '../components/printer-station'
import stations from '../models/stations'

const userPrinterStations = () => stations.get()

const Stations = {
  view () {
    return m('section.dashboard',
      userPrinterStations().length > 0
        ? userPrinterStations().map(printer => m(printerStation, {printer}))
        : m('p', 'No stations created')
    )
  }
}

export default {
  view () {
    return m(pageWrapper, m(Stations))
  }
}
