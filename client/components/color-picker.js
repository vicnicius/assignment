import m from 'mithril'

export default {
  view (vnode) {
    const {color} = vnode.attrs

    return m('input.dashboard-form-input[type="text"][placeholder="#FFFFFF"]', {
      onchange: m.withAttr('value', color),
      value: color()
    })
  }
}
