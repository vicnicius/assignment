import m from 'mithril'
import pageWrapper from '../components/page-wrapper'

const Main = {
  view () {
    return m('section.dashboard',
      m('a.button.button--large[href="/stations/new"]', { oncreate: m.route.link }, '+ create new station')
    )
  }
}

export default {
  view () {
    return m(pageWrapper, m(Main))
  }
}
