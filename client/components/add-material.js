import _ from 'lodash'
import m from 'mithril'
import stream from 'mithril/stream'
import materials from '../models/materials'
import stations from '../models/stations'
import colorPicker from './color-picker'

const getMaterialsDetails = materialIds => materials.get(materialIds)
const defaultMaterialId = stationMaterials => _.head(stationMaterials()).id
const saveMaterial = (
  stationMaterials,
  printer,
  { materialId, materialColor, materialQuantity }
) => event => {
  if (!materialId()) {
    materialId(defaultMaterialId(stationMaterials))
  }

  const materialDetails = _.head(materials.get([Number(materialId())]))
  const material = {
    id: materialDetails.id,
    name: materialDetails.name,
    color: materialColor(),
    quantity: materialQuantity()
  }

  stations.update(printer, material)
  event.preventDefault()
}

export default {
  oninit (vnode) {
    vnode.state.stationMaterials = stream([])
    vnode.state.materialId = stream()
    vnode.state.materialQuantity = stream()
    vnode.state.materialColor = stream('#FFFFFF')
  },
  view (vnode) {
    const printer = vnode.attrs.printer
    const stationMaterials = vnode.state.stationMaterials
    stationMaterials(getMaterialsDetails(printer.materials))

    return m('form.dashboard-form.printer-add-material', {
      onsubmit: saveMaterial(stationMaterials, printer, vnode.state)
    }, [
      m('label.dashboard-form-label', 'type of material:'),
      m('select.dashboard-form-select', {
        onchange: m.withAttr('value', vnode.state.materialId),
        value: vnode.state.materialId()
      }, stationMaterials().map(material => m(`option[value="${material.id}"]`, material.name))),
      m('label.dashboard-form-label', 'quantity:'),
      m('input.dashboard-form-select[type="number"][name="units"]', {
        onchange: m.withAttr('value', vnode.state.materialQuantity),
        value: vnode.state.materialQuantity()
      }),
      m('label.dashboard-form-label', 'color (in hex):'),
      m(colorPicker, {color: vnode.state.materialColor}),
      m('input.button.button--submit[type="submit"][value="+ add"]')
    ])
  }
}
