import m from 'mithril'
import printerDetails from './printer-details'

export default {
  isOpened: false,
  view (vnode) {
    const printer = vnode.attrs.printer.printer
    const materials = vnode.attrs.printer.materials
    return printer
      ? m('.printer', [
        m('.printer-title', {
          onclick: () => {
            vnode.state.isOpened = !vnode.state.isOpened
          }
        }, [
          m('i.icon-cube'),
          printer.name
        ]),
        vnode.state.isOpened ? m(printerDetails, {printer, materials}) : null
      ])
      : null
  }
}
