import m from 'mithril'

export default {
  view () {
    return m('p.footer', 'made with <3 to the 3dHubs team')
  }
}
