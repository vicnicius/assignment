# 3D Hubs Front-end assignment

On 3D Hubs, the Hubs need a way to manage their printers and the materials for those printers.
Your task is to build an interface to allow Hubs to create printing stations, associate a printer and relevant materials with the printing station, as well as relevant colors for each material and the ability to disable or remove a printing station entirely.

## User stories

**Story:** As a Hub I need to be able to set up a number of printing stations by adding printers.
**Acceptance criteria:**

- Only a single printer from the supplied list can be added by the user to each printing station.
- Printing stations can be easily removed or temporarily disabled.

**Story:** As a Hub I want to associate a number of materials with each of my printing stations.
**Acceptance criteria:**

- Only materials that can be printed on the relevant printer can be added to that printing station.
- Any colors can be added to specific materials.

## Technical information:

- Printer and material data should be retreived via calls to an API, this may be mocked or implemented as you wish.
- You only need to support the latest version of Google Chrome.
- Any framework may be used.

## Data

### Printers

    [
      {
        "id": 1,
        "name": "Form 1",
        "manufacturer": "Formlabs",
        "technology": "sla",
        "materials": [7, 8, 9],
      },
      {
        "id": 2,
        "name": "Form 2",
        "manufacturer": "Formlabs",
        "technology": "sla",
        "materials": [7, 8, 9],
      },
      {
        "id": 3,
        "name": "Ultimaker 3",
        "manufacturer": "Ultimaker",
        "technology": "fdm",
        "materials": [1, 2, 10, 11],
      },
      {
        "id": 3,
        "name": "M300",
        "manufacturer": "Zortrax",
        "technology": "fdm",
        "materials": [1, 2, 10, 11, 20, 21, 22, 23, 24],
      },
      {
        "id": 4,
        "name": "Objet 500",
        "manufacturer": "Stratasys",
        "technology": "material jetting",
        "materials": [14, 15, 16, 17, 18, 19],
      },
      {
        "id": 5,
        "name": "ProX SLS 500",
        "manufacturer": "3D Systems",
        "technology": "sls",
        "materials": [3, 4, 5, 6, 12, 13],
      },
      {
        "id": 6,
        "name": "Prusa i3",
        "manufacturer": "Prusa",
        "technology": "fdm",
        "materials": [1, 2, 10, 11],
      },
      {
        "id": 7,
        "name": "P 770",
        "manufacturer": "EOS",
        "technology": "sls",
        "materials": [3, 4, 5, 6, 12, 13],
      },
    ]

### Materials

    [
      {
        "id": 1,
        "name": "ColorFabb PLA",
      },
      {
        "id": 2,
        "name": "ColorFabb WoodFill",
      },
      {
        "id": 3,
        "name": "DuraForm Flex",
      },
      {
        "id": 4,
        "name": "DuraForm GF",
      },
      {
        "id": 5,
        "name": "DuraForm HST",
      },
      {
        "id": 6,
        "name": "DuraForm PA",
      },
      {
        "id": 7,
        "name": "Formlabs Clear Resin",
      },
      {
        "id": 8,
        "name": "Formlabs Standard Resin",
      },
      {
        "id": 9,
        "name": "Formlabs Tough Resin",
      },
      {
        "id": 10,
        "name": "MatterHackers ABS",
      },
      {
        "id": 11,
        "name": "Polymaker PolyMax PLA",
      },
      {
        "id": 12,
        "name": "Nylon 12",
      },
      {
        "id": 13,
        "name": "Nylon 11"
      },
      {
        "id": 14,
        "name": "Rigur",
      },
      {
        "id": 15,
        "name": "TangoBlackPlus",
      },
      {
        "id": 16,
        "name": "TangoPlus",
      },
      {
        "id": 17,
        "name": "VeroBlue",
      },
      {
        "id": 18,
        "name": "VeroClear",
      },
      {
        "id": 19,
        "name": "VeroWhitePlus",
      },
      {
        "id": 20,
        "name": "Z-ABS",
      },
      {
        "id": 21,
        "name": "Z-Glass",
      },
      {
        "id": 22,
        "name": "Z-HIPS",
      },
      {
        "id": 23,
        "name": "Z-PETG",
      },
      {
        "id": 24,
        "name": "Z-ULTRAT",
      },
    ]
