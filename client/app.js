import m from 'mithril'
import Main from './pages/main'
import Stations from './pages/stations'
import StationsNew from './pages/stations-new'
import printers from './models/printers'
import materials from './models/materials'

printers.fetch()
materials.fetch()
m.route.prefix('')
m.route(document.getElementById('app'), '/', {
  '/': Main,
  '/stations/new': StationsNew,
  '/stations': Stations
})
