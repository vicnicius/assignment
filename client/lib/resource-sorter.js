export default (resourceA, resourceB) => {
  if (resourceA.name < resourceB.name) return -1
  if (resourceA.name > resourceB.name) return 1
  return 0
}
