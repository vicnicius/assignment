import m from 'mithril'
import _ from 'lodash'
import stream from 'mithril/stream'
import resourceSorter from '../lib/resource-sorter'

const materialsModel = {
  all: stream([]),
  get (ids) {
    return _.filter(
        materialsModel.all(),
        material => _.includes(ids, material.id)
    )
  },
  fetch () {
    if (materialsModel.all().length > 0) return
    m.request('/api/materials').then(materials => {
      const sortedMaterials = materials.sort(resourceSorter)
      materialsModel.all(sortedMaterials)
    })
  }
}

export default materialsModel
