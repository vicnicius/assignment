import m from 'mithril'
import _ from 'lodash'
import stream from 'mithril/stream'
import resourceSorter from '../lib/resource-sorter'

const printersModel = {
  all: stream([]),
  get (id) {
    return _.find(printersModel.all(), {id: Number(id)})
  },
  fetch () {
    if (printersModel.all().length > 0) return
    m.request('/api/printers').then(printers => {
      const sortedPrinters = printers.sort(resourceSorter)
      printersModel.all(sortedPrinters)
    })
  }
}

export default printersModel
