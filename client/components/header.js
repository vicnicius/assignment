import m from 'mithril'

export default {
  view () {
    return m('header',
      m('nav',
        m('ul.navigation', [
          m('li.navigation-item', m('a.navigation-item-logo[href="/"]', { oncreate: m.route.link },
            m('img[src="/static/img/logo.svg"][width="115"]')
          )),
          m('li.navigation-item', m('a.navigation-item-link[href="/stations/new"]', { oncreate: m.route.link },
            '/create'
          )),
          m('li.navigation-item', m('a.navigation-item-link[href="/stations"]', { oncreate: m.route.link },
            '/manage'
          ))
        ])
      )
    )
  }
}
