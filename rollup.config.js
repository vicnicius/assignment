import resolve from 'rollup-plugin-node-resolve'
import commonjs from 'rollup-plugin-commonjs'

export default {
    input: 'client/app.js',
    output: {
        file: 'static/js/bundle.js',
        format: 'iife'
    },
    sourcemap: true,
    plugins: [ resolve(), commonjs()]    
}