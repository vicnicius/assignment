import _ from 'lodash'
import m from 'mithril'
import stream from 'mithril/stream'
import pageWrapper from '../components/page-wrapper'
import stations from '../models/stations'
import printers from '../models/printers'

const selectedPrinter = stream()
const defaultPrinterValue = () => _.head(printers.all()).id
const save = event => {
  if (!selectedPrinter()) {
    selectedPrinter(defaultPrinterValue())
  }
  stations.create(selectedPrinter())
  m.route.set('/stations')
  event.preventDefault()
}

const StationsNew = {
  view (vnode) {
    return m('section.dashboard', [
      m('h1.dashboard-title', 'Create new station'),
      m('form.dashboard-form[action="/api/stations/new"][method="POST"]', {onsubmit: save}, [
        m('label.dashboard-form-label[for="printer"]', 'Choose printer model'),
        printers.all().length > 0
          ? m('select.dashboard-form-select#printer', {
            onchange: m.withAttr('value', selectedPrinter),
            value: selectedPrinter()
          },
            printers.all().map(printer => m(`option.dashboard-form-option[value="${printer.id}"]`, printer.name))
          )
          : m('span', 'loading printers...'),
        m('input.button.dashboard-form-submit[type="submit"][value="save"]')
      ])
    ])
  }
}

export default {
  view () {
    return m(pageWrapper, m(StationsNew))
  }
}
