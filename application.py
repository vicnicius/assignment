from flask import Flask, render_template, jsonify

app = Flask(__name__)

@app.route('/')
@app.route('/stations/new')
@app.route('/stations/manage')
@app.route('/stations')
def index():
    return render_template('index.html')

@app.route('/api/printers')
def get_printers():
    return jsonify([
      {
        "id": 1,
        "name": "Form 1",
        "manufacturer": "Formlabs",
        "technology": "sla",
        "materials": [7, 8, 9],
      },
      {
        "id": 2,
        "name": "Form 2",
        "manufacturer": "Formlabs",
        "technology": "sla",
        "materials": [7, 8, 9],
      },
      {
        "id": 3,
        "name": "Ultimaker 3",
        "manufacturer": "Ultimaker",
        "technology": "fdm",
        "materials": [1, 2, 10, 11],
      },
      {
        "id": 4,
        "name": "M300",
        "manufacturer": "Zortrax",
        "technology": "fdm",
        "materials": [1, 2, 10, 11, 20, 21, 22, 23, 24],
      },
      {
        "id": 5,
        "name": "Objet 500",
        "manufacturer": "Stratasys",
        "technology": "material jetting",
        "materials": [14, 15, 16, 17, 18, 19],
      },
      {
        "id": 6,
        "name": "ProX SLS 500",
        "manufacturer": "3D Systems",
        "technology": "sls",
        "materials": [3, 4, 5, 6, 12, 13],
      },
      {
        "id": 7,
        "name": "Prusa i3",
        "manufacturer": "Prusa",
        "technology": "fdm",
        "materials": [1, 2, 10, 11],
      },
      {
        "id": 8,
        "name": "P 770",
        "manufacturer": "EOS",
        "technology": "sls",
        "materials": [3, 4, 5, 6, 12, 13],
      },
    ])

@app.route('/api/materials')
def get_materials():
  return jsonify([
      {
        "id": 1,
        "name": "ColorFabb PLA",
      },
      {
        "id": 2,
        "name": "ColorFabb WoodFill",
      },
      {
        "id": 3,
        "name": "DuraForm Flex",
      },
      {
        "id": 4,
        "name": "DuraForm GF",
      },
      {
        "id": 5,
        "name": "DuraForm HST",
      },
      {
        "id": 6,
        "name": "DuraForm PA",
      },
      {
        "id": 7,
        "name": "Formlabs Clear Resin",
      },
      {
        "id": 8,
        "name": "Formlabs Standard Resin",
      },
      {
        "id": 9,
        "name": "Formlabs Tough Resin",
      },
      {
        "id": 10,
        "name": "MatterHackers ABS",
      },
      {
        "id": 11,
        "name": "Polymaker PolyMax PLA",
      },
      {
        "id": 12,
        "name": "Nylon 12",
      },
      {
        "id": 13,
        "name": "Nylon 11"
      },
      {
        "id": 14,
        "name": "Rigur",
      },
      {
        "id": 15,
        "name": "TangoBlackPlus",
      },
      {
        "id": 16,
        "name": "TangoPlus",
      },
      {
        "id": 17,
        "name": "VeroBlue",
      },
      {
        "id": 18,
        "name": "VeroClear",
      },
      {
        "id": 19,
        "name": "VeroWhitePlus",
      },
      {
        "id": 20,
        "name": "Z-ABS",
      },
      {
        "id": 21,
        "name": "Z-Glass",
      },
      {
        "id": 22,
        "name": "Z-HIPS",
      },
      {
        "id": 23,
        "name": "Z-PETG",
      },
      {
        "id": 24,
        "name": "Z-ULTRAT",
      },
    ])